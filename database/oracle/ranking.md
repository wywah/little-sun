---
title: oracle 使用 rank 、dense_rank 函数进行数据排名
date: 2020-09-17
tags: 
 - database
categories: 
 - database
publish: true
description: oracle 排名函数
sidebar: 'auto'
# sticky: 1
---
:::tip
**oracle 排名函数:**
  两种排名方式(分区和不分区):
    使用和不使用 **partition**
  两种计算方式(连续,不连续)
    连续计算：**dense_rank**
    不连续计算：**rank** ;
**语法:**
  rank() over (orderby排序字段 顺序)
  rank() over (**partition by** 分组字段 **order by** 排序字段 顺序)
:::

```sql
create table T_SCORE
(
  s_id     VARCHAR2(32),
  s_name   VARCHAR2(32),
  sub_name VARCHAR2(32),
  score    NUMBER
);
insert into T_SCORE (s_id, s_name, sub_name, score)
values ('1', '张三', '语文', 80);
insert into T_SCORE (s_id, s_name, sub_name, score)
values ('2', '李四', '数学', 80);
insert into T_SCORE (s_id, s_name, sub_name, score)
values ('1', '张三', '数学', 0);
insert into T_SCORE (s_id, s_name, sub_name, score)
values ('2', '李四', '语文', 50);
insert into T_SCORE (s_id, s_name, sub_name, score)
values ('3', '张三丰', '语文', 10);
insert into T_SCORE (s_id, s_name, sub_name, score)
values ('3', '张三丰', '数学', null);
insert into T_SCORE (s_id, s_name, sub_name, score)
values ('3', '张三丰', '体育', 120);
insert into T_SCORE (s_id, s_name, sub_name, score)
values ('4', '杨过', 'JAVA', 90);
insert into T_SCORE (s_id, s_name, sub_name, score)
values ('5', 'mike', 'c++', 80);
insert into T_SCORE (s_id, s_name, sub_name, score)
values ('3', '张三丰', 'Oracle', 0);
insert into T_SCORE (s_id, s_name, sub_name, score)
values ('4', '杨过', 'Oracle', 77);
insert into T_SCORE (s_id, s_name, sub_name, score)
values ('2', '李四', 'Oracle', 77);
```

## 查询各学生科目为*Oracle*排名(简单排名)

```sql
select t.s_id,t.s_name,t.sub_name,t.score,rank()over(order by t.score desc) 排名
        from t_score t where t.sub_name = 'Oracle'
```

![result](./imgs/rank-1.jpg)

可发现分数存相同时，排名就不连续了

**使用 dense_rank 进行排名**

```sql
select t.s_id,t.s_name,t.sub_name,t.score,dense_rank ()over(order by t.score desc) 排名
        from t_score t where t.sub_name = 'Oracle'
```

![result](./imgs/rank-2.jpg)

这样的话就发现排名是连续的了。

## 查询各学生各科排名(分区排名)

```sql
select t.s_id,t.s_name,t.sub_name,t.score,rank()over(partition by t.sub_name order by t.score desc) 排名
from t_score t;
select t.s_id,t.s_name,t.sub_name,t.score,dense_rank()over(partition by t.sub_name order by t.score desc) 排名
from t_score t;
```

![result](./imgs/rank-3.png)

![result](./imgs/rank-4.png)

## 总分排名

```sql
select t.s_name,t.score,rank()over(order by t.score) 排名
from (select t.s_name,sum(t.score) score from t_score t group by t.s_name) t;
select t.s_name,t.score,dense_rank()over(order by t.score) 排名
from (select t.s_name,sum(t.score) score from t_score t group by t.s_name) t;
```

![result](./imgs/rank-5.png)

![result](./imgs/rank-6.png)







