---
title: oracle 一条数据根据特定字符拆分成多条数据
date: 2022-07-31
tags: 
 - database
categories: 
 - database
publish: true
description: oracle 字符串拆分
sidebar: 'auto'
# sticky: 1
---
:::tip
Oracle 拆分字符串：  
    根据某个字段以及特定的字符串进行多条数据拆分；
:::
1. 首先我们来看一下数据格式：  
    ![数据格式](./imgs/mult_substr_1.png)
2. 我们需要的得到的结果是：  
    ![理想结果](./imgs/mult_substr_2.png)
3. 查阅资料我们可以发现regexp_substr ... connect by，使用正则表达式进行拆分字符串为多条数据；  
   如下图：  
   ![示例1](./imgs/mult_substr_3.png)
4. 分析 sql function  
    **regexp_substr(str,pattern,position,occurrence,modifier)：**  
        - str:  需要进行正则处理的字符串  
        - pattern:  进行匹配的正则表达式  
        - position:  起始位置，从字符串的第几个字符开始正则表达式匹配（默认为1） 注意：字符串最初的位置是1而不是0  
        - occurrence:  获取第几个分割出来的组（分割后最初的字符串会按分割的顺序排列成组）  
        - modifier:  模式（‘i’不区分大小写进行检索；‘c’区分大小写进行检索。默认为’c’）针对的是正则表达式里字符大小写的匹配  
    **connect by level<= n ：**  
        - 把要输出来的第几个子串，通过一个变量level转换成输出多少个子串。level<=5代表的是输出5个  
5. 由此我们写出sql进行测试  
    ![示例2](./imgs/mult_substr_4.png)  
    但发现数据会出现重复现象(目前还不清楚什么原因)， 没关系，我们可以通过去重来规避，如下：  
    ![示例3](./imgs/mult_substr_5.png)   
6. 考虑到效率问题，上面的SQL有待提高，而且存在数据重复的问题，只能再次修改：  
    由于我们知道 cs2 字段最多只会有5个字符，所以生成5个序列，关联查询后由于两个表中的xb不一致，所以就可以把regexp_substr中occurrence为空的的数据去除  
    ![示例4](./imgs/mult_substr_6.png)  
7. 最终我们的SQL是：  
    ```sql
        select regexp_substr(cs2,'[^,]+',1,xb,'i') as cs2,cs1,cs3 from test01 t ,  
            (select level xb from dual connect by level <= 5 ) r  
        where xb <= length(cs2) - length(regexp_replace(cs2,',','')) + 1 ;  
    ```
