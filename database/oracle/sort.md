---
title: 排序 oreder by
date: 2021-03-12
tags: 
 - database
categories: 
 - database
publish: true
description: 排序 oreder by 高阶使用
sidebar: 'auto'
# sticky: 1
---
:::tip 说明
    排序 oreder by 高阶使用  
    使用 case when 实现不同条件采用不同的排序规则
:::
1. 常规使用
```sql
select * from [table_name] order by [column1] desc,[column2] asc;
```
结果按照 字段一 降序 字段二顺序排序。

2. 高阶使用
```sql
select * from [table_name] order by [column1] desc,
    case when [column2] == '1' then [column3] end asc,case when [column2] == '2' then [column4] end desc ;
```
结果按照 某字段 降序 字段二为‘1’ 的 根据某字段 顺序排序，字段二为‘2’ 的 根据某字段 降序排序， 。



:::warning 警告
1）ORDER BY的字段改到一种表、不要夸表（设计表结构时需注意这一点）  

2）OEDER BY字段建索引、多个字段时建联合索引（联合索引的字段顺序要与ORSER BY中的字段顺序一致）  

3）ORDER BY中字段中联合索引的所有字段DESC或ASC要统一，否则索引不起作用  

4）不要对TEXT字段或者CLOB字段进行排序  
:::