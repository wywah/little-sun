---
home: true
heroText: 爱慧
tagline: 博客
heroImage: ./avatar.jpg
heroImageStyle: {
  maxWidth: '600px',
  width: '30%',
  display: block,
  #margin: '9rem auto 2rem',
  borderRadius: '7rem',
}
bgImageStyle: {
  #background: 'url("./2.jpg") center center / cover no-repeat',
  background: 'url("./home-bg.jpg") center center / cover no-repeat',
  opacity:0.5,
  height: '914px'
}
isShowTitleInHome: true
actionText: Guide
actionLink: /views/other/guide
features:
- title: Yesterday
  details: 开发一款看着开心、写着顺手的 vuepress 博客主题
- title: Today
  details: 希望帮助更多的人花更多的时间在内容创作上，而不是博客搭建上
- title: Tomorrow
  details: 希望更多的爱好者能够参与进来，帮助这个主题更好的成长
footer: MIT Licensed | Copyright © 2018-present Evan You
---


