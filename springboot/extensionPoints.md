---
title: SpringBoot 可扩展点
date: 2020-08-08 10:00:00
tags: 
 - SpringBoot
categories: 
 - SpringBoot
publish: true
description: SpringBoot 可扩展点
# sticky: 1
---
# SpringBoot 可扩展点
> SpringBoot 扩展点 ， 通过实现相应接口
## SmartInitializingSingleton
> 在spring容器管理的所有单例对象（非懒加载对象）初始化完成之后调用的回调接口  
> 其触发时机为 postProcessAfterInitialization之后  
```java
/**
 * 在spring容器管理的所有单例对象（非懒加载对象）初始化完成之后调用的回调接口
 * 其触发时机为 postProcessAfterInitialization之后
 */
@Configuration
public class CustomSmartInitializingSingleton implements SmartInitializingSingleton {
    private static Logger logger = LoggerFactory.getLogger(CustomSmartInitializingSingleton.class);
    @Override
    public void afterSingletonsInstantiated() {
        logger.info("在spring容器管理的所有单例对象（非懒加载对象）初始化完成之后调用的回调接口， {}",CustomSmartInitializingSingleton.class.getName());
    }
}

## result ==========================
2022-02-17 17:34:07.819  INFO 19800 --- [           main] o.s.c.a.ConfigurationClassPostProcessor  : Cannot enhance @Configuration bean definition 'customBeanDefinitionRegistryPostProcessor' since its singleton instance has been created too early. The typical cause is a non-static @Bean method with a BeanDefinitionRegistryPostProcessor return type: Consider declaring such methods as 'static'.
2022-02-17 17:34:07.881  INFO 19800 --- [           main] ustomBeanDefinitionRegistryPostProcessor : ==== postProcessBeanFactory ====
2022-02-17 17:34:08.178  INFO 19800 --- [           main] o.s.b.w.embedded.tomcat.TomcatWebServer  : Tomcat initialized with port(s): 9000 (http)
2022-02-17 17:34:08.178  INFO 19800 --- [           main] o.apache.catalina.core.StandardService   : Starting service [Tomcat]
2022-02-17 17:34:08.178  INFO 19800 --- [           main] org.apache.catalina.core.StandardEngine  : Starting Servlet engine: [Apache Tomcat/9.0.55]
2022-02-17 17:34:08.272  INFO 19800 --- [           main] o.a.c.c.C.[Tomcat].[localhost].[/]       : Initializing Spring embedded WebApplicationContext
2022-02-17 17:34:08.272  INFO 19800 --- [           main] w.s.c.ServletWebServerApplicationContext : Root WebApplicationContext: initialization completed in 950 ms
2022-02-17 17:34:08.571  INFO 19800 --- [           main] c.f.a.e.CustomSmartInitializingSingleton : 在spring容器管理的所有单例对象（非懒加载对象）初始化完成之后调用的回调接口， com.fenyue.analysis.extenstionPoints.CustomSmartInitializingSingleton
2022-02-17 17:34:08.618  INFO 19800 --- [           main] o.s.b.w.embedded.tomcat.TomcatWebServer  : Tomcat started on port(s): 9000 (http) with context path ''
2022-02-17 17:34:08.618  INFO 19800 --- [           main] .a.s.CustomApplicationContextInitializer : ==== [com.fenyue.analysis.spiExtension.CustomApplicationListener] ====
2022-02-17 17:34:08.618  INFO 19800 --- [           main] c.f.a.spiExtension.CustomAppStartUp      : === [com.fenyue.analysis.spiExtension.CustomAppStartUp],应用已启动成功 ===
2022-02-17 17:34:08.634  INFO 19800 --- [           main] com.fenyue.analysis.AnalysisApplication  : Started AnalysisApplication in 1.984 seconds (JVM running for 3.024)
2022-02-17 17:34:08.634  INFO 19800 --- [           main] f.a.s.CustomSpringApplicationRunListener : ==== Spring上下文已加载完毕但Spring Boot Run的回调还没有执行之前回调 CustomSpringApplicationRunListener.started ====
2022-02-17 17:34:08.634  INFO 19800 --- [           main] com.fenyue.analysis.event.EmailNotifier  : 开始触发发送邮件事件... ...
2022-02-17 17:34:08.634  INFO 19800 --- [           main] com.fenyue.analysis.event.EmailNotifier  : 获取 Email Context: [EmailContext{address='1050744@qq.com', context='Hello World'}]
2022-02-17 17:34:08.634  INFO 19800 --- [           main] f.a.s.CustomSpringApplicationRunListener : ==== Spring Boot启动成功执行该回调 CustomSpringApplicationRunListener.ready ====
```