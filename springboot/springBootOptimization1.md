# 优化SpringBoot项目
:::tip 提示
springboot 内置容器所以只能进行线程、连接数等信息进行调优配置  
可以根据场景进行慢慢调优，找到一个合适的配置  
```
server:
  tomcat:
    threads:
      max: 200
      min-spare: 20
    connection-timeout: 5000
```
tomcat进行了一个优化配置，最大线程数是200，初始化线程是20,超时时间是5000ms
:::
## JVM 的调优
一般来说在server端运行都会指定如下参数  
:::tip
初始内存和最大内存基本会设置成一样的，具体大小根据场景设置,  
-server是一个必须要用的参数，至于收集器这些使用默认的就可以了，除非有特定需求。
:::
1. **使用-server模式**
设置JVM使用server模式。64位JDK默认启动该模式
```
java -server -jar springboot-1.0.jar
```
2. **指定堆参数**
这个根据服务器的内存大小，来设置堆参数。  
- -Xms :设置Java堆栈的初始化大小
- -Xmx :设置最大的java堆大小
```
java -server -Xms512m -Xmx768m  -jar springboot-1.0.jar
```
设置初始化堆内存为512MB，最大为768MB。
3. **远程Debug**
在服务器上将启动参数修改为：
```
java -Djavax.net.debug=ssl -Xdebug -Xnoagent   
-Djava.compiler=NONE -Xrunjdwp:transport=dt_socket,server=y,
suspend=n,address=8888 -jar springboot-1.0.jar
```
这个时候服务端远程Debug模式开启，端口号为8888。在IDEA中，点击Edit Configuration按钮
4. JVM工具远程连接
- **jconsole与Jvisualvm远程连接**
```
java -jar -Djava.rmi.server.hostname=192.168.44.128  
-Dcom.sun.management.jmxremote -Dcom.sun.management.jmxremote.port=911  
-Dcom.sun.management.jmxremote.ssl=false   
-Dcom.sun.management.jmxremote.authenticate=false jantent-1.0-SNAPSHOT.jar
```