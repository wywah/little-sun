---
date: 2022-02-17 00:30:00
---
# ApplicationListener 事件使用示例

1. 创建自定义事件
```java
/**
 * 邮件内容
 */
public class EmailContext {
    private String address;
    private String context;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getContext() {
        return context;
    }

    public void setContext(String context) {
        this.context = context;
    }

    @Override
    public String toString() {
        return "EmailContext{" +
                "address='" + address + '\'' +
                ", context='" + context + '\'' +
                '}';
    }

    public EmailContext(String address, String context) {
        this.address = address;
        this.context = context;
    }

    public EmailContext() {
    }
}
/**
 * 自定义发送电子邮件事件
 */
public class EmailEvent extends ApplicationEvent {
    public EmailEvent(EmailContext source) {
        super(source);
    }

    public final EmailContext getEmailContext() {
        return (EmailContext)this.getSource();
    }
}
```
2. 监听事件以及出发事件
```java
/**
 * ApplicationContextAware ： 用来获取上下文
 * ApplicationListener ： 监听邮件事件
 * CommandLineRunner ： 服务启动后触发邮件事件
 */
@Configuration
public class EmailNotifier implements CommandLineRunner, ApplicationContextAware, ApplicationListener<EmailEvent> {
    private static Logger logger = LoggerFactory.getLogger(EmailNotifier.class);
    private ApplicationContext applicationContext;
    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }

    @Override
    public void onApplicationEvent(EmailEvent event) {
        EmailContext context = event.getEmailContext();
        logger.info("获取 Email Context: [{}]", context.toString());
    }

    @Override
    public void run(String... args) throws Exception {
        EmailContext context = new EmailContext("1050744@qq.com","Hello World");
        EmailEvent emailEvent = new EmailEvent(context);
        logger.info("开始触发发送邮件事件... ...");
        applicationContext.publishEvent(emailEvent);
    }
}
```
### 结果
```
2022-02-17 00:27:34.816  INFO 21788 --- [           main] com.fenyue.analysis.event.EmailNotifier  : 开始触发发送邮件事件
2022-02-17 00:27:34.816  INFO 21788 --- [           main] com.fenyue.analysis.event.EmailNotifier  : 获取 Email Context: [EmailContext{address='1050744@qq.com', context='Hello World'}]

```
