---
title: SpringBoot 启动流程解析
date: 2022-02-16 23:00:00
tags: 
 - SpringBoot
categories: 
 - SpringBoot
publish: true
description: 源码学习
# sticky: 1
---
# SpringBoot 启动流程解析
> 基于 SpringBoot 2.6.0 版本分析  
> [示例代码](https://gitee.com/wyw_miss/spring-boot-analysis)
- [搭建环境](./sourceAnalysis/springboot/setout)
- [分析 SpringBoot 启动过程中存在的一些扩展点](./sourceAnalysis/springboot/extensionPoints)
