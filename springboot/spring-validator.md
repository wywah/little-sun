---
title: SpringBoot Validator 参数校验
date: 2022-08-08 22:12:00
tags: 
 - SpringBoot
categories: 
 - SpringBoot
publish: true
description: SpringBoot自定义参数校验
# sticky: 1
---
# SpringBoot自定义参数校验
> 示例基于 SpringBoot-2.6.1；  
> SpringBoot版本号小于 2.3.x 时，无需额外引用依赖 ， 反之需要引用 spring-boot-starter-validation 依赖；  
> 主要是实现 ConstraintValidator 接口并重写isValid方法， 如果isValid方法返回false时反之通过校验，会触发MethodArgumentNotValidException异常,后续可通过异常信息中的defaultMessage获取注解中message的信息  
> ConstraintValidator实现类指定的注解需要有message、groups、payload等属性；  
> 开启参数校验可通过 @Validated或者@Valid
## 触发校验器的注解
```java
@Documented
@Constraint(validatedBy = {RegValidator.class}) // 使用哪个类处理验证逻辑
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.ANNOTATION_TYPE, ElementType.CONSTRUCTOR, ElementType.PARAMETER, ElementType.TYPE_USE})
@Retention(RetentionPolicy.RUNTIME)
@Repeatable(RegFy.List.class) // 同一个地方使用相同的注解
public @interface RegFy {
    // 正则表达式
    String reg();
    // 异常信息
    String message() default "{com.fenyue.analysis.validator.annotations.RegFy.message}";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
    @Target({ElementType.METHOD, ElementType.FIELD, ElementType.ANNOTATION_TYPE, ElementType.CONSTRUCTOR, ElementType.PARAMETER, ElementType.TYPE_USE})
    @Retention(RetentionPolicy.RUNTIME)
    @Documented
    public @interface List {
        RegFy[] value();
    }
}
```
## 自定义校验器
```java
public class RegValidator implements ConstraintValidator<RegFy , CharSequence> {
    private Logger logger = LoggerFactory.getLogger(RegValidator.class);
    private CharSequence reg;
    private String message;
    @Override
    public void initialize(RegFy regFy) {
        this.reg = regFy.reg();
        this.message = regFy.message();
    }
    // 如果返回false时，会触发MethodArgumentNotValidException异常， 反之通过校验
    @Override
    public boolean isValid(CharSequence value, ConstraintValidatorContext constraintValidatorContext) {
        if(value == null){
            return true;
        }else {
            logger.info("reg：[{}] ; message:[{}]",this.reg,this.message);
            return false;
        }

    }
}
```
## 验证
```java
## 实体类
public class SayVo {
    @RegFy(message = "不合法",reg="/d")
    @RegFy(message = "不合法",reg="/w")
    @NotBlank(message = "dsd")
    private String message;
    public String getMessage() {
        return message;
    }
    public void setMessage(String message) {
        this.message = message;
    }
}

## 
@RestController
public class TestController {
    @PostMapping("say")
    public String say(@Validated @RequestBody SayVo req){
        return req.getMessage();
    }
}
```
## Spring官方文档
:::tip Spring官方文档 
[https://docs.spring.io/spring-framework/docs/5.3.x/reference/html/core.html#validation-beanvalidation-spring-constraints](https://docs.spring.io/spring-framework/docs/5.3.x/reference/html/core.html#validation-beanvalidation-spring-constraints)
:::
