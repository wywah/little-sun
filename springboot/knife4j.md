---
title: SpringBoot 整合 knife4j
date: 2020-08-08
tags: 
 - SpringBoot
categories: 
 - SpringBoot
publish: true
description: 自定义SpringBoot starts启动包
sidebar: 'auto'
# sticky: 2
---
:::tip
knife4j 是 swagger2 的 UI 增强版
:::
# SpringBoot 整合 knife4j

​	1、swagger2 增强版  ， 主要时 ui 增强 ； 访问地址：  xxx.xx.xx.xx:xx/doc.html

​	2、[swagger API](https://swagger.io/docs)        https://swagger.io/docs 

​	3、 [Knife4j API](https://doc.xiaominfo.com/)         https://doc.xiaominfo.com/

## maven 坐标 ：

```pom
<!-- https://mvnrepository.com/artifact/com.github.xiaoymin/knife4j-spring-boot-starter -->
<dependency>
   <groupId>com.github.xiaoymin</groupId>
   <artifactId>knife4j-spring-boot-starter</artifactId>
   <version>2.0.4</version>
</dependency>
```

## yml 配置说明 ：

```yml
knife4j:
  production: false #是否关闭 swagger ; 默认开启
  #markdowns: #markdowns地址
  basic:
    enable: true #是否开启 swagger 登录认证 ; 默认关闭
    username: admin # 用户名
    password: admin # 密码

```

## Knife4j 模板配置 ：

### SwaggerConfiguration.class 配置文件

```java
import com.fasterxml.classmate.ResolvedType;
import com.fasterxml.classmate.TypeResolver;
import com.github.xiaoymin.knife4j.spring.annotations.EnableKnife4j;
import com.google.common.collect.Lists;
import com.wyw.knife4j.config.knif4j.DeveloperApiInfo;
import com.wyw.knife4j.config.knif4j.DeveloperApiInfoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.ParameterBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.*;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.List;

/**
 * @Des knif4j 增强 swagger 接口文档配合
 * @Author player
 * @DateTime 2020/8/5 10:21
 * @Version 1.0
 */
@Configuration
//开启swagger2
@EnableSwagger2
//开启knif4j增强
@EnableKnife4j
//@Import(BeanValidatorPluginsConfiguration.class)
public class SwaggerConfiguration {

    private TypeResolver typeResolver;

    @Autowired
    public SwaggerConfiguration(TypeResolver typeResolver) {
        this.typeResolver = typeResolver;
    }

    @Bean(value = "defaultApi")
    public Docket defaultApi() {
        ParameterBuilder parameterBuilder=new ParameterBuilder();
        List<Parameter> parameters= Lists.newArrayList();
        parameterBuilder.name("token").description("token令牌").modelRef(new ModelRef("String"))
                .parameterType("header")
                .required(true).build();
        parameters.add(parameterBuilder.build());
        Docket docket=new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .groupName("默认接口") // 文档分组名称
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.wyw.knife4j.controller.contro1"))
                //.apis(RequestHandlerSelectors.withMethodAnnotation(ApiOperation.class)) //拦截注解@ApiOperation
                .paths(PathSelectors.any())
                .build().globalOperationParameters(parameters) //注册全局参数
                .securityContexts(Lists.newArrayList(securityContext())).securitySchemes(Lists.<SecurityScheme>newArrayList(apiKey()));
        return docket;
    }
    
    // API 的配置信息
    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("RESTful APIs") // 标题； 不支持 html
                .description("#RESTful APIs")   // 描述； 支持 html
                .termsOfServiceUrl("https://doc.xiaominfo.com/") //服务地址
                .contact(new Contact("wyw","https://doc.xiaominfo.com/","lisi@163.com")) //作者信息
                .version("1.0") //版本信息
                .build();
    }


    @Bean(value = "groupRestApi")
    public Docket groupRestApi() {
        List<ResolvedType> list=Lists.newArrayList();

        //SpringAddtionalModel springAddtionalModel= springAddtionalModelService.scan("com.swagger.bootstrap.ui.demo.extend");
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(groupApiInfo())
                .groupName("分组接口")
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.wyw.knife4j.controller.contro2"))
                .paths(PathSelectors.any())
                .build()
                .additionalModels(typeResolver.resolve(DeveloperApiInfo.class))
                .securityContexts(Lists.newArrayList(securityContext(),securityContext1()))
                .securitySchemes(Lists.<SecurityScheme>newArrayList(apiKey(),apiKey1()));
    }

    private ApiInfo groupApiInfo(){
        DeveloperApiInfoExtension apiInfoExtension=new DeveloperApiInfoExtension();

        apiInfoExtension.addDeveloper(new DeveloperApiInfo("张三","zhangsan@163.com","Java"))
                .addDeveloper(new DeveloperApiInfo("李四","lisi@163.com","Java"));


        return new ApiInfoBuilder()
                .title("文档标题")
                .description("<div style='font-size:14px;color:red;'>第二版 RESTful APIs</div>")
                .termsOfServiceUrl("http:127.0.0.1:8999")
                .license("231213")
                .contact(new Contact("wyw","https://doc.xiaominfo.com/","lisi@163.com"))
                .version("1.0")
                .extensions(Lists.newArrayList(apiInfoExtension))
                .build();
    }

    private ApiKey apiKey() {
        return new ApiKey("BearerToken", "Authorization", "header");
    }
    private ApiKey apiKey1() {
        return new ApiKey("BearerToken1", "Authorization-x", "header");
    }

    private SecurityContext securityContext() {
        return SecurityContext.builder()
                .securityReferences(defaultAuth())
                .forPaths(PathSelectors.regex("/.*"))
                .build();
    }
    private SecurityContext securityContext1() {
        return SecurityContext.builder()
                .securityReferences(defaultAuth1())
                .forPaths(PathSelectors.regex("/.*"))
                .build();
    }

    List<SecurityReference> defaultAuth() {
        AuthorizationScope authorizationScope = new AuthorizationScope("global", "accessEverything");
        AuthorizationScope[] authorizationScopes = new AuthorizationScope[1];
        authorizationScopes[0] = authorizationScope;
        return Lists.newArrayList(new SecurityReference("BearerToken", authorizationScopes));
    }
    List<SecurityReference> defaultAuth1() {
        AuthorizationScope authorizationScope = new AuthorizationScope("global", "accessEverything");
        AuthorizationScope[] authorizationScopes = new AuthorizationScope[1];
        authorizationScopes[0] = authorizationScope;
        return Lists.newArrayList(new SecurityReference("BearerToken1", authorizationScopes));
    }
}

```

### DeveloperApiInfoExtension.class

```java
package com.wyw.knife4j.config.knif4j;

import springfox.documentation.service.VendorExtension;

import java.util.ArrayList;
import java.util.List;

/**
 * @Des TODO
 * @Author player
 * @DateTime 2020/8/5 10:31
 * @Version 1.0
 */
public class DeveloperApiInfoExtension implements VendorExtension<List<DeveloperApiInfo>> {
    private final String EXTEND_API_INFO="x-developers";

    private List<DeveloperApiInfo> developerApiInfoExtensions=new ArrayList<>();

    public DeveloperApiInfoExtension addDeveloper(DeveloperApiInfo developerApiInfo){
        developerApiInfoExtensions.add(developerApiInfo);
        return this;
    }

    @Override
    public String getName() {
        return EXTEND_API_INFO;
    }

    @Override
    public List<DeveloperApiInfo> getValue() {
        return developerApiInfoExtensions;
    }
}
```

### DeveloperApiInfo.class

```java
package com.wyw.knife4j.config.knif4j;

/**
 * @Des TODO
 * @Author player
 * @DateTime 2020/8/5 10:28
 * @Version 1.0
 */
public class DeveloperApiInfo {
    private String name;

    private String email;

    private String role;
    
    // ## get and set function ##
}
```

## =============================== end =============================

## swagger2 starter 启动包 ：

### maven 坐标 ：

```pom
<dependency>
  <groupId>io.springfox</groupId>
  <artifactId>springfox-boot-starter</artifactId>
  <version>3.0.0</version>
</dependency>
```

### API ：

https://github.com/springfox/springfox

http://springfox.github.io/springfox/