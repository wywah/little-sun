---
title: SpringBoot启动时的扩展点
date: 2022-02-16 23:00:00
---
> SpringBoot 启动时都有哪些扩展点
## 1. SpringBoot项目的启动入口
```java
@SpringBootApplication
public class AnalysisApplication {
    public static void main(String[] args) {
        SpringApplication.run(AnalysisApplication.class, args);
    }
}
```
### 1.1 SpringApplication.run 都做了哪些工作?
> SpringApplication初始化并调用run()方法  
> 1. 根据org.springframework.web.reactive.DispatcherHandler、org.springframework.web.servlet.DispatcherServlet、org.glassfish.jersey.servlet.ServletContainer这三个特征类初始化WebApplicationType 的类型（Spring上下文的类型）
> 2. 设置默认的上下文工厂 ； 
> 3. 从classpath目录下加载BootstrapRegistryInitializer、ApplicationContextInitializer、ApplicationListener的实现类 ; 然后执行 run 方法 ， run方法  
> 4. 回调 BootstrapRegistryInitializer.initialize() 和 SpringApplicationRunListener.starting();   
> 5. 准备运行环境、Banner，根据createApplicationContext()创建Spring上下文  
> 6. 执行 prepareContext 方法开始准备上下文；设置环境配置、执行ApplicationContextInitializer.initialize()、SpringApplicationRunListener.contextPrepared()、BootstrapRegistryInitializer.close()、SpringApplicationRunListener.contextLoaded()回调，并把@EnableAutoConfiguration获取到的配置以及其他形式的IoC容器配置加载到准备就绪的Spring上下文中，到此Spring上下文已经准备完毕  
> 7. 调用ApplicationContext.refresh()方法，完成IoC容器可用的最后一道工序.  
> 8. 执行 ApplicationRunner、CommandLineRunner回调  
> 9. 如果程序执行正常回调SpringApplicationRunListener.ready()否则回调SpringApplicationRunListener.failed()
> 追踪源码可知 SpringApplication.run方法最终执行的是SpringBootApplication.run方法 
> 而SpringBootApplication初始化时会对SpringBoot应用做一些初始化的配置，代码如下：  
  ```java
    public SpringApplication(ResourceLoader resourceLoader, Class<?>... primarySources) {
        this.sources = new LinkedHashSet();
        this.bannerMode = Mode.CONSOLE;
        this.logStartupInfo = true;
        this.addCommandLineProperties = true;
        this.addConversionService = true;
        this.headless = true;
        this.registerShutdownHook = true;
        this.additionalProfiles = Collections.emptySet();
        this.isCustomEnvironment = false;
        this.lazyInitialization = false;
        this.applicationContextFactory = ApplicationContextFactory.DEFAULT;
        this.applicationStartup = ApplicationStartup.DEFAULT;
        this.resourceLoader = resourceLoader;
        Assert.notNull(primarySources, "PrimarySources must not be null");
        this.primarySources = new LinkedHashSet(Arrays.asList(primarySources));
        this.webApplicationType = WebApplicationType.deduceFromClasspath();
        // 通过Spring SPI 加载 BootstrapRegistryInitializer 接口实现类
        this.bootstrapRegistryInitializers = new ArrayList(this.getSpringFactoriesInstances(BootstrapRegistryInitializer.class));
        // 通过Spring SPI 加载 ApplicationContextInitializer 接口实现类
        this.setInitializers(this.getSpringFactoriesInstances(ApplicationContextInitializer.class));
        // 通过Spring SPI 加载 ApplicationListener 接口实现类
        this.setListeners(this.getSpringFactoriesInstances(ApplicationListener.class));
        this.mainApplicationClass = this.deduceMainApplicationClass();
    }
  ```

### 1.1.1 接口扩展

### 1.1.2 Spring SPI 扩展
> 使用“\”进行换行 ， 多个实现类使用“,”分割  
- BootstrapRegistryInitializer 接口扩展
    > 在BootstrapRegistry被使用之前回调该实现类
    ```spring.factories
    # spring.factories
    ## CustomBootstrapRegistryInitializer
    org.springframework.boot.BootstrapRegistryInitializer=\
        com.fenyue.analysis.spiExtension.CustomBootstrapRegistryInitializer

    ## java
    public class CustomBootstrapRegistryInitializer implements BootstrapRegistryInitializer {
        private static Logger logger = LoggerFactory.getLogger(CustomBootstrapRegistryInitializer.class);
        @Override
        public void initialize(BootstrapRegistry registry) {
            logger.info("执行[{}]",CustomBootstrapRegistryInitializer.class);
        }
    }
    ```
- ApplicationContextInitializer 接口扩展
    > 用于在spring容器刷新之前初始化Spring ConfigurableApplicationContext的回调接口。（就是在容器刷新之前调用该类的 initialize 方法。并将 ConfigurableApplicationContext 类的实例传递给该方法）  
    > 通常用于需要对应用程序上下文进行编程初始化的web应用程序中。例如，根据上下文环境注册属性源或激活配置文件等  
    ```spring.factories
    # spring.factories
    ## CustomApplicationContextInitializer
    org.springframework.context.ApplicationContextInitializer=\
        com.fenyue.analysis.spiExtension.CustomApplicationContextInitializer

    ## java
    public class CustomApplicationContextInitializer implements ApplicationContextInitializer {
        private static Logger logger = LoggerFactory.getLogger(CustomApplicationContextInitializer.class);
        @Override
        public void initialize(ConfigurableApplicationContext applicationContext) {
            logger.info("==== [{}] ====",CustomApplicationContextInitializer.class.getName());
        }
    }
    ```
- ApplicationListener 接口扩展
    > SpringBoot 事件监听  
    > ApplicationListener 内置事件 包含：ContextRefreshedEvent、ContextStartedEvent、ContextStoppedEvent、ContextClosedEvent、RequestHandledEvent 分别对应  
    > [使用示例](../../examples/ApplicationListener)
    ```spring.factories
    # spring.factories
    ## CustomApplicationListener
    org.springframework.context.ApplicationListener=\
    com.fenyue.analysis.spiExtension.CustomApplicationListener,\
    com.fenyue.analysis.spiExtension.CustomAppStartUp

    # java
    public class CustomAppStartUp implements ApplicationListener<ContextRefreshedEvent> {
        private static Logger logger = LoggerFactory.getLogger(CustomAppStartUp.class);
        @Override
        public void onApplicationEvent(ContextRefreshedEvent event) {
            logger.info("=== [{}],应用已启动成功 ===",CustomAppStartUp.class.getName());
        }
    }
    ```
- SpringApplicationRunListener 接口扩展
    > 该接口规定了SpringBoot应用的生命周期，在各个生命周期广播相应的事件，实际调用ApplicationListener类
    ```spring.factories
    # spring.factories
    ## CustomSpringApplicationRunListener
    org.springframework.boot.SpringApplicationRunListener=\
    com.fenyue.analysis.spiExtension.CustomSpringApplicationRunListener

    # java
    public class CustomApplicationContextInitializer implements ApplicationContextInitializer {
        private static Logger logger = LoggerFactory.getLogger(CustomApplicationContextInitializer.class);
        @Override
        public void initialize(ConfigurableApplicationContext applicationContext) {
            logger.info("==== [{}] ====",CustomApplicationContextInitializer.class.getName());
        }
    }
    ```

### 1.2 @SpringBootApplication 注解都做了哪些工作?
> SpringBootApplication 是一个组合注解
```java
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
@SpringBootConfiguration // 等价于 @Configuration
@EnableAutoConfiguration // 开启自动配置
/**
 * Spring MVC 自动扫描注解 ， 对应Spring XML配置 @Component、@Repository
 * 或者bean方式配置的实例BEAN;
 * basePackages等属性来细粒度的定制@ComponentScan自动扫描的范围，如果不指定，
 * 则默认Spring框架实现会从声明@ComponentScan所在类的package进行扫描;
 * 所以SpringBoot默认扫描 @SpringBootApplication所在类的文件目录
 */
@ComponentScan(
    excludeFilters = {@Filter(
    type = FilterType.CUSTOM,
    classes = {TypeExcludeFilter.class}
), @Filter(
    type = FilterType.CUSTOM,
    classes = {AutoConfigurationExcludeFilter.class}
)}
) 
public @interface SpringBootApplication {}
```
### 1.2.1 @EnableAutoConfiguration 启动自动配置
> 该注解会引用两个类 ， AutoConfigurationImportSelector、Registrar  
> 借助@Import的支持，收集和注册特定场景相关的bean定义; 将所有符合自动配置条件的bean定义加载到IoC容器
```java
## EnableAutoConfiguration
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
@AutoConfigurationPackage
@Import({AutoConfigurationImportSelector.class})
public @interface EnableAutoConfiguration {}

## AutoConfigurationPackage
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
@Import({Registrar.class})
public @interface AutoConfigurationPackage {}
```

### 1.2.2 @ComponentScan
> Spring MVC 自动扫描注解 ， 对应Spring XML配置 @Component、@Repository或者bean方式配置的实例BEAN;  
>  basePackages等属性来细粒度的定制@ComponentScan自动扫描的范围，如果不指定，则默认Spring框架实现会从声明@ComponentScan所在类的package进行扫描;  
> 所以SpringBoot默认扫描 @SpringBootApplication所在类的文件目录 
```java
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
@Documented
@Repeatable(ComponentScans.class)
public @interface ComponentScan {
    @AliasFor("basePackages")
    String[] value() default {};

    @AliasFor("value")
    String[] basePackages() default {};

    /**
     * 对 basePackages 指定扫描注释组件包类型安全的代替
     */
    Class<?>[] basePackageClasses() default {};

    /**
     *
     */
    Class<? extends BeanNameGenerator> nameGenerator() default BeanNameGenerator.class;

    /**
     * 解决检测到的组件的范围
     */
    Class<? extends ScopeMetadataResolver> scopeResolver() default AnnotationScopeMetadataResolver.class;

    /**
     *
     */
    ScopedProxyMode scopedProxy() default ScopedProxyMode.DEFAULT;

    String resourcePattern() default "**/*.class";

    /**
     * 是否自动检测类的注解
     */
    boolean useDefaultFilters() default true;

    /**
     * 指定哪些类型组件可以扫描
     */
    ComponentScan.Filter[] includeFilters() default {};

    /**
     * 指定哪些类型组件不可以扫描
     */
    ComponentScan.Filter[] excludeFilters() default {};

    /**
     * 扫描注册的组件为lazy初始化
     */
    boolean lazyInit() default false;

    @Retention(RetentionPolicy.RUNTIME)
    @Target({})
    public @interface Filter {
        FilterType type() default FilterType.ANNOTATION;

        @AliasFor("classes")
        Class<?>[] value() default {};

        @AliasFor("value")
        Class<?>[] classes() default {};

        String[] pattern() default {};
    }
}
```
