---
title: 搭建案例环境
date: 2022-02-16 23:00:00
---
# 准备工作
> 搭建 SpringBoot 2.6.1环境 案例环境  
## 创建聚合项目
> 使用 IDEA 创建聚合项目 , 父项目只管理依赖;  
> 创建父项目以及example子项目;  
```pom.xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 https://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>
    <groupId>com.fenyue.analysis</groupId>
    <artifactId>SpringBootAnalysis</artifactId>
    <packaging>pom</packaging>
    <version>0.0.1-SNAPSHOT</version>
    <modules>
        <module>examples</module>
    </modules>
    <name>SpringBootAnalysis</name>
    <description>SpringBootAnalysis</description>
    <properties>
        <java.version>17</java.version>
        <spring.boot.version>2.6.1</spring.boot.version>
    </properties>
    <dependencyManagement>
        <dependencies>
            <!-- SpringBoot的依赖配置 -->
            <dependency>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-dependencies</artifactId>
                <version>${spring.boot.version}</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>
        </dependencies>
    </dependencyManagement>
</project>
```
