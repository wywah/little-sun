---
title: 条件注解
date: 2020-08-08 10:00:00
tags: 
 - SpringBoot
categories: 
 - SpringBoot
publish: true
description: 自定义SpringBoot starts启动包
# sticky: 1
---

## @ConditionalOnProperty
> 通过@ConditionalOnProperty控制配置类是否生效,可以将配置与代码进行分离,实现了更好的控制配置.  
> @ConditionalOnProperty实现是通过havingValue与配置文件中的值对比,返回为true则配置类生效,反之失效.  
```java
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE, ElementType.METHOD})
@Documented
@Conditional({OnPropertyCondition.class})
public @interface ConditionalOnProperty {
    /**
     * String数组  该属性与下面的 name 属性不可同时使用，
     * 当value所对应配置文件中的值为false时，注入不生效，不为fasle注入生效
     * value有多个值时，只要有一个值对应为false,则注入不成功      
     */
    String[] value() default {};
    /**
     * 配置文件中key的前缀，可与value 或 name 组合使用 
     */
    String prefix() default "";

    /**
     * 与 value 作用一致
     */
    String[] name() default {};
    /**
     * 与value 或 name 组合使用，只有当value 或 name 
     * 对应的值与havingValue的值相同时，注入生效 
     * String havingValue() default "";
     * 配置中缺少对应的属性时，是否可以被注入；
     * 为true时缺少对应配置可注入 
     * */
    String havingValue() default "";
    /**
     * 该属性为true时，配置文件中缺少对应的value或name的对应的属性值，
     * 也会注入成功 
     */
    boolean matchIfMissing() default false;
}
```
该注解中 name和value必须指定一个

## @ConditionalOnBean
> 	Spring容器中是否存在对应的实例。可以通过实例的类型、类名、注解、昵称去容器中查找(可以配置从当前容器中查找或者父容器中查找或者两者一起查找)这些属性都是数组，通过”与”的关系进行查找
```java
@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Conditional({OnBeanCondition.class})
public @interface ConditionalOnBean {
    Class<?>[] value() default {};

    String[] type() default {};

    Class<? extends Annotation>[] annotation() default {};

    String[] name() default {};

    SearchStrategy search() default SearchStrategy.ALL;

    Class<?>[] parameterizedContainer() default {};
}
```