---
title: 自定义SpringBoot starts启动包
date: 2020-08-08 10:00:00
tags: 
 - SpringBoot
categories: 
 - SpringBoot
publish: true
description: 自定义SpringBoot starts启动包
# sticky: 1
---
# 自定义spring-boot-starts启动包

## 命名规则

1、xx-spring-boot-starter

2、xx-spring-boot-starter 依赖 xx-spring-boot-autoconfigure

## 自动配置生效方式

### 主动生效自动配置
> 使用注解中的@Import触发自动配置
```java
/**
 * 主动开启自动配置
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Import({TestHelloAutoConfigure.class})  //导入自动配置类
public @interface EnableHelloServer {
}

可以在springboot项目的启动类上加入 @EnableHelloServer ，即可启动
@SpringBootApplication
@EnableHelloServer  //开启 HelloServer 自动配置
public class CacheApplication {
    public static void main(String[] args) {
        SpringApplication.run(CacheApplication.class,args);
    }
}
```

### 被动生效自动配置
> springboot 开启自动配置时，会自动扫描各个包下的/META-INF/spring.factories文件中的
> 详细可参考 SpringFactoriesLoader.Class 对 spring.factories 的解析
```spring.factories
# Auto Configure
org.springframework.boot.autoconfigure.EnableAutoConfiguration=\
com.wyw.autoconfigure.hello.TestHelloAutoConfigure
```

### 自动配置项目增加提示信息
> 打包会自动生成配置文件中的配置提示  
> optional=true 使maven引用时不影响项目
```java
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-configuration-processor</artifactId>
    <optional>true</optional>
</dependency>
```

### 自动配置包必须引用项目
```java
<parent>
  <groupId>org.springframework.boot</groupId>
  <artifactId>spring-boot-starter-parent</artifactId>
  <version>2.3.1.RELEASE</version>
  <relativePath/> <!-- lookup parent from repository -->
</parent>
    
<dependency>
   <groupId>org.springframework.boot</groupId>
   <artifactId>spring-boot-starter</artifactId>
</dependency>
```