---
title: 概览
sidebar: true
date: 2020-8-10
sidebarDepth: 0
---
:::tip
spring-boot 版本基于 2.2.9.RELEASE、spring-cloud 版本基于 Hoxton.SR7 、 spring-cloud-alibaba 版本基于 2.2.1.RELEASE
:::
# Spring Cloud 一站式 微服务解决方案学习笔记
1. 服务中心 nacos
    1.1 服务注册&发现 sping-cloud-alibaba-nacos
2. 网关 sping-cloud-gateway
3. 配置中心 sping-cloud-alibaba-config
