---
title: 网关 sping-cloud-gateway
date: 2020-08-17 10:00:00
tags: 
 - spring cloud
categories: 
 - spring cloud
# publish: true
description: 服务注册&发现
# sticky: 1
---
:::tip
sping-cloud-gateway 是spring-cloud新一代网关 , 详细文档请移步[gateway](https://spring.io/projects/spring-cloud-gateway#learn)
:::