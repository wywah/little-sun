---

title: sping-cloud-gateway 聚合 swagger2 API 文档
date: 2020-08-17 12:56:00
tags: 
 - spring cloud
categories: 
 - sping-cloud-gateway
# publish: true
description: 网关
# sticky: 1
---
::: tip
该例子整合swagger2的增强版knife4j
:::
::: warning
注意swagger2对spring不同版本的basePath的处理，具体查看源码中Swagger2Controller.getDocumentation()
:::

## 各微服务swagger配置

### Swagger2Config.java

```java
/**
 * @Des knif4j 增强 swagger 接口文档配合
 * @Author player
 * @DateTime 2020/8/14 14:53
 * @Version 1.0
 */
@Configuration
//开启swagger2
@EnableSwagger2
//开启knif4j增强
@EnableKnife4j
public class Swagger2Config {
    private TypeResolver typeResolver;
    @Autowired
    public Swagger2Config(TypeResolver typeResolver) {
        this.typeResolver = typeResolver;
    }
    @Bean("restful-api")
    public Docket defaultApi() {
        Docket docket=new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                //.groupName("基础架构API")  //如存在分组信息，需对应修改网关服务的请求方式
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.weni.management.controller"))
                //.apis(RequestHandlerSelectors.withMethodAnnotation(ApiOperation.class))
                .paths(PathSelectors.any())
                .build();
        return docket;
    }
    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("basics RESTful APIs")
                .description("<div style='font-size:14px;color:red;'>management RESTful APIs</div>")
                .termsOfServiceUrl("https://127.0.0.1")
                .contact(new Contact("wyw","https://doc.xiaominfo.com/","****@163.com"))
                .version("1.0")
                .build();
    }

}

```

### manager配置文件 

```yml
spring:
  application:
    name: weni-manager
  cloud:
    nacos:
      discovery:
        server-addr: 127.0.0.1:8848  // 服务注册进 nacos 服务中心
      server-addr: 127.0.0.1:9903
server:
  port: 9903
  servlet:
    context-path: /manager
```

## gateway 网关配置

### 配置文件 application.yml

```yml
spring:
  application:
    name: weni-gateway
  cloud:
    nacos:
      discovery:
        server-addr: 127.0.0.1:8848
      server-addr: 127.0.0.1:9902
    gateway:
      globalcors: #全局跨域
        cors-configurations:
          '[/**]':
            allowedHeader: "*"
            allowedOrigins: "*"
            allowedMethods: "*"
            allowCredentials: true
      x-forwarded:
        prefix-enabled: false # 去掉默认设置  x-forwarded-prefix
      routes:  #配置路由路径
        - id: weni-generator-api
          uri: lb://weni-generator
          predicates:
            - Path=/api/generator/**
          filters:
            - SwaggerHeaderFilter
            - RewritePath=/api/(?<remaining>.*),/${remaining}
        - id: weni-manager-api
          uri: lb://weni-manager
          predicates:
            - Path=/api/manager/**
          filters:
            - SwaggerHeaderFilter
            - RewritePath=/api/(?<remaining>.*),/${remaining}
      discovery:
        locator:
          enabled: true #开启从注册中心动态创建路由的功能
          lower-case-service-id: true #使用小写服务名，默认是大写
server:
  port: 9902
  servlet:
    context-path: /gateway
```

> 由于 gateway 存在全局的filter默认设置 x-forwarded-prefix 为 path ， 所以需要全局禁用或者局部禁止，本例子采用全局禁用，详情请查看官网API

### 网关处 swagger 配置类 Swagger2Config.java

```java
@Component
@Primary
public class Swagger2Config implements SwaggerResourcesProvider {

    private final RouteLocator routeLocator;  // gateway
    private final GatewayProperties gatewayProperties;

    private Logger log = LoggerFactory.getLogger(Swagger2Config.class);

    public Swagger2Config(RouteLocator routeLocator, GatewayProperties gatewayProperties) {
        this.routeLocator = routeLocator;
        this.gatewayProperties = gatewayProperties;
    }

    @Override
    public List<SwaggerResource> get() {
        List<SwaggerResource> resources = new ArrayList<SwaggerResource>();
        List<String> routes = new ArrayList<String>();
        //获取所有路由的ID
        routeLocator.getRoutes().subscribe(route -> routes.add(route.getId()));
        //过滤出配置文件中定义的路由->过滤出Path Route Predicate->根据路径拼接成api-docs路径->生成SwaggerResource
        gatewayProperties.getRoutes().stream().filter(routeDefinition -> routes.contains(routeDefinition.getId())).forEach(route -> {
            route.getPredicates().stream()
                    .filter(predicateDefinition -> ("Path").equalsIgnoreCase(predicateDefinition.getName()))
                    .forEach(predicateDefinition -> resources.add(swaggerResource(route.getId(),
                            predicateDefinition.getArgs().get(NameUtils.GENERATED_NAME_PREFIX + "0")
                                    .replace("**", "v2/api-docs"))));
        });

        return resources;
    }

    private SwaggerResource swaggerResource(String name, String location) {
        log.info("name:{},location:{}", name, location);
        SwaggerResource swaggerResource = new SwaggerResource();
        swaggerResource.setName(name);
        swaggerResource.setLocation(location);
        swaggerResource.setSwaggerVersion("2.0");
        return swaggerResource;
    }

    public RouteLocator getRouteLocator() {
        return routeLocator;
    }

    public GatewayProperties getGatewayProperties() {
        return gatewayProperties;
    }
}
```

### 配置获取Swagger资源的接口

```java
@RestController
public class SwaggerHandler {

    @Autowired(required = false)
    private SecurityConfiguration securityConfiguration;

    @Autowired(required = false)
    private UiConfiguration uiConfiguration;

    private final SwaggerResourcesProvider swaggerResources;

    @Autowired
    public SwaggerHandler(SwaggerResourcesProvider swaggerResources) {
        this.swaggerResources = swaggerResources;
    }

    /**
     * Swagger安全配置，支持oauth和apiKey设置
     */
    @GetMapping("/swagger-resources/configuration/security")
    public Mono<ResponseEntity<SecurityConfiguration>> securityConfiguration() {
        return Mono.just(new ResponseEntity<>(
                Optional.ofNullable(securityConfiguration).orElse(SecurityConfigurationBuilder.builder().build()), HttpStatus.OK));
    }

    /**
     * Swagger UI配置
     */
    @GetMapping("/swagger-resources/configuration/ui")
    public Mono<ResponseEntity<UiConfiguration>> uiConfiguration() {
        return Mono.just(new ResponseEntity<>(
                Optional.ofNullable(uiConfiguration).orElse(UiConfigurationBuilder.builder().build()), HttpStatus.OK));
    }

    /**
     * Swagger资源配置，微服务中这各个服务的api-docs信息
     */
    @GetMapping("/swagger-resources")
    public Mono<ResponseEntity> swaggerResources() {
        return Mono.just((new ResponseEntity<>(swaggerResources.get(), HttpStatus.OK)));
    }
}
```

到此整合已接近尾声，还需解决网关代理后各个为服务的swagger的basePath的问题

通过查看swagger源码：

![Swagger2Controller.java](./imgs/2.jpg)

![HostNameProvider.java](./imgs/3.jpg)

```XForwardPrefixPathAdjuster.java
public class XForwardPrefixPathAdjuster implements PathAdjuster {
    static final String X_FORWARDED_PREFIX = "X-Forwarded-Prefix";
    private final HttpServletRequest request;

    public XForwardPrefixPathAdjuster(HttpServletRequest request) {
        this.request = request;
    }

    public String adjustedPath(String path) {
        String prefix = this.request.getHeader("X-Forwarded-Prefix");
        if (prefix != null) {
            return !SpringVersionCapability.supportsXForwardPrefixHeader(SpringVersion.getVersion()) ? prefix + path : prefix;
        } else {
            return path;
        }
    }
}
```

由以上可知解决方案 ，只需设置Header("X-Forwarded-Prefix")即可；在网关服务中增加 filter

```SwaggerHeaderFilter.java
@Component
public class SwaggerHeaderFilter extends AbstractGatewayFilterFactory {
    private static final String HEADER_NAME = "X-Forwarded-Prefix";

    private static final String URI = "/v2/api-docs";
    @Override
    public GatewayFilter apply(Object config) {
        return (exchange, chain) -> {
            ServerHttpRequest request = exchange.getRequest();
            String path = request.getURI().getPath();
            if (!StringUtils.endsWithIgnoreCase(path,URI)) {
                return chain.filter(exchange);
            }
            String basePath = path.substring(0, path.lastIndexOf(URI));

            ServerHttpRequest newRequest = request.mutate().header(HEADER_NAME, basePath).build();
            ServerWebExchange newExchange = exchange.mutate().request(newRequest).build();
            return chain.filter(newExchange);
        };
    }
}
```

yml配置增如下：

```yml
spring:
  application:
    name: weni-gateway
  cloud:
    nacos:
      discovery:
        server-addr: 127.0.0.1:8848
      server-addr: 127.0.0.1:9902
    gateway:
      globalcors: #全局跨域
        cors-configurations:
          '[/**]':
            allowedHeader: "*"
            allowedOrigins: "*"
            allowedMethods: "*"
            allowCredentials: true
      x-forwarded:
        prefix-enabled: false # 去掉默认设置  x-forwarded-prefix
      routes:  #配置路由路径
        - id: weni-generator-api
          uri: lb://weni-generator
          predicates:
            - Path=/api/generator/**
          filters:
            - SwaggerHeaderFilter
            - RewritePath=/api/(?<remaining>.*),/${remaining}
        - id: weni-manager-api
          uri: lb://weni-manager
          predicates:
            - Path=/api/manager/**
          filters:
            - SwaggerHeaderFilter
            - RewritePath=/api/(?<remaining>.*),/${remaining}
      discovery:
        locator:
          enabled: true #开启从注册中心动态创建路由的功能
          lower-case-service-id: true #使用小写服务名，默认是大写
server:
  port: 9902
  servlet:
    context-path: /gateway
```

#### 到此配置结束，可访问 ip:port/doc.html访问api

![doc.html](./imgs/1.png)

