::: tip

Spring-Cloud-Alibaba 版本基于 2.2.1.RELEASE 

:::

# Spring-Cloud-Alibaba-Config 配置中心

## 首先引用依赖

```pom
<dependency>
    <groupId>com.alibaba.cloud</groupId>
    <artifactId>spring-cloud-starter-alibaba-nacos-config</artifactId>
</dependency>
```

::: warning

使用nacos-config时，配置文件激活、文件名等信息需要配置在 bootstrap.yml/properties文件中 ， 如使用实时数据 需使用  @RefreshScope 注解标注	

```yml
spring:
  application:
    name: weni-gateway
  profiles:
    active: dev # 激活${spring.application.name}-${spring.profiles.active}.${spring.cloud.nacos.config.file-extension}
  cloud:
    nacos:
      ## nacos config 配置中心
      config:
        file-extension: yaml # 指定配置文件类型
        server-addr: 127.0.0.1:8848 # 文件配置中心
        refresh-enabled: true # 默认开启自动刷新
        enabled: true # 开启配置中心
        namespace: weni  # 指定 配置中心的命名空间 ，默认public
        group: weni # 指定 配置中心的分组
      discovery:
        server-addr: 127.0.0.1:8848 # 服务中心
      server-addr: 127.0.0.1:9902 # 服务地址
```

:::

