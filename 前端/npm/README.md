---
title: npm 常用包
date: 2021-1-26
sidebarDepth: 0
---
::: tip 说明
个人收录npm包列表
:::
## 综合
1. escape-html : 转译html 
2. chalk : node 控制台输出不同颜色
3. prismjs ： 代码高亮, 美化