---
title: icon 图标
date: 2021-1-26
sidebarDepth: 0
---
::: tip
使用阿里图标库做icon图标，使其像element-plus那样引入icon
:::
1. **在阿里图标库创建项目**  
![icon_1](./images/icon_1.png)  
***需要注意，fontclass 属性，后续需要使用。***  
2. **往项目里添加自己所需要的图标**  
![icon_2](./images/icon_2.png)  
***然后点击下载或者生成线上图标css文件***  
3. **文件解压后**  
![icon_3](./images/icon_3.png)  
4. **修改iconfont.css**  
![icon_4](./images/icon_4.png)  
***azure-icon、azure-icon- 是图1说设置的font-family和fontClass属性值***  
好了这样就可以使用了  
```js
<i class="azure-icon-guanbi2"></i>
<i class="azure-icon azure-icon-guanbi2"></i>
```