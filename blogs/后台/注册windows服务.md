---
title: springboot jar 注册成windows服务
date: 2020-12-11
tags: 
 - 博客
categories: 
 - java
 - windows
 - springboot
description: springboot jar 以服务的形式在windows服务上运行
---

# springboot jar 以服务的形式在windows服务上运行

> 使用 windows-server-wapper 【[winsw](https://github.com/winsw/winsw)】将一个可执行文件封装成windows的服务

1. **github -> https://github.com/winsw/winsw 下载 winsw**

![WinSW-1](https://wyw_miss.gitee.io/imgs/blogs/winsw/winsw-1.png)



![winsw-2](https://wyw_miss.gitee.io/imgs/blogs/winsw/winsw-2.png)

2. **下载后需要修改一下WinSW的配置文件以及执行文件名，方便后续敲命令**

   - **修改前**：

   ![update-1](https://wyw_miss.gitee.io/imgs/blogs/winsw/winsw-3.png)

   - **修改后**：

   ![update-2](https://wyw_miss.gitee.io/imgs/blogs/winsw/winsw-4.png)

   - **修改WinSW配置文件server.xml**：

   ![server.xml](https://wyw_miss.gitee.io/imgs/blogs/winsw/winsw-5.png)

3. cmd 命令进入该文件目录下执行命令，命令就是修改的WinSW文件名

```
# 注册成服务的命令
server install
# 卸载命令
server uninstall
# 其他命令可至github中的文档中查看 或者 查看 sample-allOptions.xml 文件中的配置进行修改也可。
```

4. 执行 services.msc 在服务栏中即可查询注册的服务。