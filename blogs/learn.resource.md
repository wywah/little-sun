---
title: 程序开发参考网站
date: 2022-02-17
tags: 
 - 博客
 - 文档
categories: 
 - Java
 - vue
publish: true
description: 程序开发参考网站
sidebar: 'auto'
sticky: 3  
---
:::tip
个人日常工作收集的学习、资料网站
:::
## Java
> Java 语言相关
- [Spring Cloud中文网](https://www.springcloud.cc/)
- [Spring官网](https://spring.io/)
- [Spring Boot 快速搭建平台](https://start.spring.io/)
- [Maven Package Search](https://package-search.jetbrains.com/)
    
## Go
> Go 语言相关
- [Go语言中文文档](https://www.topgoer.com/)

## Vue 2.x
- [vuejs 2.x 官网](https://cn.vuejs.org/)
- [VuePress 静态网站](https://www.vuepress.cn/)
- [vuepress-theme-reco VuePress主题](https://vuepress-theme-reco.recoluan.com/)
- [NuxtJs](https://nuxtjs.org/)
- [vue插件库大全](https://www.jianshu.com/p/0ef292736c9b)

## Vue 3.x
- [vuejs 3 官网](https://vuejs.org/)
- [vuejs 3 官网(中文)](https://staging-cn.vuejs.org/)
- [vuejs 3 聚合网](https://vue3js.cn/)
- [Vue Router 4](https://router.vuejs.org/)
- [Vuex4](https://vuex.vuejs.org/)
- [Pinia vue3数据状态库，替换vuex4](https://pinia.vuejs.org/)
- [Naive UI](https://www.naiveui.com/zh-CN/os-theme)
- [ElementPlus](https://element-plus.gitee.io/zh-CN/#/zh-CN)
- [vitepress 静态网站](https://vitepress.vuejs.org/)
- [VueUse](https://vueuse.org/)
- [Nuxt3](https://v3.nuxtjs.org/)
- [vuedraggable vue3拖拽组件](https://sortablejs.github.io/vue.draggable.next/#/simple)
- [wangeditor 富文本编辑器](https://www.wangeditor.com/)

## 前端构建工具
- [Vite](https://vitejs.dev/)

## JavaScript
- [Web 开发技术](https://developer.mozilla.org/zh-CN/docs/Web)
- [菜鸟教程](https://www.runoob.com/)
- [Lodash 中文文档](https://www.lodashjs.com/)
- [TypeScript 中文文档](https://www.tslang.cn/)
- [Webpack 中文文档](https://www.webpackjs.com/)
- [Electron 文档](https://www.electronjs.org/docs)
- [Apache ECharts 一个基于 JavaScript 的源可视化图表库 ](https://echarts.apache.org/zh/index.html)
- [npm 官网](https://www.npmjs.com/)
- [Axios 中文说明](https://www.kancloud.cn/yunye/axios/234845)
- [前端喵](http://blog.51weblove.com/)
- [查找当前流行的npm库](https://npm.devtool.tech/)

## 前端
- [BgRemover - 在线图片去底工具 - 将纯色背景的图片转换为背景透明的图片](https://www.aigei.com/bgremover)
- [图片背景消除](https://www.remove.bg/zh)
- [LOGO 在线制作](http://www.logofree.cn/)
- [favicon 在线制作](https://tool.lu/favicon/)
- [创客贴 平面设计](https://m.chuangkit.com/)
- [网页配色](https://flatuicolors.com/)
- [中国色](http://zhongguose.com/)
- [阿里矢量图标库](https://www.iconfont.cn/)
- [渐变色网](https://webgradients.com/https://webgradients.com/)
- [Animista 动画](https://animista.net/)

## 其他
- [脚本之家](https://www.jb51.net/)
- [大学资源网](http://www.dxzy163.com/)
- [在线转换器 各种文件格式间转化](https://cn.office-converter.com/)
- [掘金](https://juejin.cn/)



