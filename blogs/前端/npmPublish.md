---
title: npm 发布自己的npm包
date: 2021-01-07
tags: 
 - 博客
categories: 
 - npm
publish: true
description: npm 发布自己的npm包 以及 管理包
sidebar: 'auto'
sticky: 2
---
:::tip
 使用npm发布自己的库，可以使用 npm i 来安装依赖  
:::

## 1. npm 账号
首先需要一个npm的[官方账号](https://www.npmjs.com/)
## 2. 创建一个前端项目
1. 创建项目并初始化

    ```npm
        mkdir jkTo  
        cd jkTo  
        npm init
    ```
    生成的package.json文件如下：
    ```json
        {
        "name": "jkto", // 包名
        "version": "1.0.0",  // 包的版本
        "description": "this is fengyue_567 test package",   // 包的描述
        "main": "index.js",   // 包的入口文件
        "scripts": { // 脚本命令
            "test": "echo \"Error: no test specified\" && exit 1"
        },
        // 被搜索的条件
        "keywords": [  
            "tool"
        ],
        "author": "fengyue",  // 作者
        "license": "ISC"
        }
    ```
    ![初始化](./npm_img/npm_init.png)
2. 创建package.json声明的入口文件 ， 本示例指定的是 index.js文件
    ```javascript
        export const Say = () => {
            console.log('Say Hello Word');
        }
    ```
3. 测试项目中导出的方法能正常使用后，准备发布
    > 在该项目根目录执行npm命令
    ```node
        # 登录npm账号
        npm login

        # 发布 公开的包 ， 不加access时默认是私有包
        npm publish --access=public

        # 更新版本号
        npm version patch

        # 再次执行 npm publish --access=public 命令即可发布更新后的包，切记包名不要更换

        # 撤销已发布的包
        # npm unpublish [包名] -> 撤销已发布的包
        # npm unpublish [包名] [--force]  -> 强制撤销已发布的包
        # npm unpublish [包名]@[版本号]  -> 撤销已发布的指定版本的包
        npm unpublish [包名]@[版本号] [--force]
    ```
    ![发布](./npm_img/npm_publish.png)
4. 在npm官网搜索该包
    ![result](./npm_img/npm_select.png)

好了，到此npm发布属于自己的包已经完成了:tada::100:。