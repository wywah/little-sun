---
title: node 开发环境搭建
date: 2020-12-30
tags: 
 - 博客
 - 文档
categories: 
 - node
publish: true
description: 当前页面的描述
sidebar: 'auto'
# prev: vuepress
# next: vuepress-theme-reco搭建个人博客
---
::: tip
搭建nodejs开发环境、npm以及yarn安装
:::
# NodeJs

## 1. 安装

1. 下载nodeJs安装包

   下载地址：http://nodejs.cn/download/ ； 

   一路下一步即可安装成功。

2. 判断是否安装成功

   ```
   # node 版本
   node -v
   ```

# npm

## 查看版本号

```
# 由于 node 高版本自带 npm
npm -v
```

## 配置环境变量

```
npm config set prefix "D:\Node.js\node-global"
npm config set cache "D:\Node.js\node-cache"
```





# yarn

## 1. 安装

1. 下载安装包

   下载地址： https://classic.yarnpkg.com/ ；

2.  使用 npm 命令安装

   ```
   npm i yarn -g
   -i：install 
   -g：全局安装（global）,使用 -g 或 --global
   ```

   

3.  配置环境

   ```
   # 设置全局安装文件
   yarn config set global-folder "E:\java\Node\yarn-global"
   # 设置缓存文件
   yarn config set cache-folder "E:\java\Node\yarn-cache"
   # 配置环境
   
   ```