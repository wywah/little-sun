# Vite 

[Vite](https://github.com/vitejs/vite) 、[vite 文档](https://vite.wiki/api/vitejs/) 、[Vite-demo](https://github.com/lovelycs/ViteJS-demo) 、[DApp 开发指南](https://vite.wiki/zh/tutorial/contract/dapp.html)

## 1、初始化项目

```npm
$ npm init vite-app <project-name>
$ cd <project-name>
$ npm install
$ npm run dev
```

## 2、安装 vue-router 、vuex 、sacss

```npm
npm i vue-router@next
npm i vuex@next
npm i sass --save-dev
```

## 3、增加  vite.config.js

```vite.config.js
module.exports = {
  // 导入别名
  // 这些条目可以是精确的请求->请求映射*（精确，无通配符语法）
  // 也可以是请求路径-> fs目录映射。 *使用目录映射时
  // 键**必须以斜杠开头和结尾**
  alias: {
    // ‘react‘: ‘@pika/react‘,
    // ‘react-dom‘: ‘@pika/react-dom‘
    // ‘/@foo/‘: path.resolve(__dirname, ‘some-special-dir‘),
  },
  // 配置Dep优化行为
  optimizeDeps: {
    // exclude: [‘dep-a‘, ‘dep-b‘],
  },
  // 转换Vue自定义块的功能。
  vueCustomBlockTransforms: {
    // i18n: src => `export default Comp => { ... }`,
  },
  // 为开发服务器配置自定义代理规则。
  proxy: {
    // proxy: {
    //   ‘/foo‘: ‘http://localhost:4567/foo‘,
    //   ‘/api‘: {
    //     target: ‘http://jsonplaceholder.typicode.com‘,
    //     changeOrigin: true,
    //     rewrite: path => path.replace(/^\/api/, ‘‘),
    //   },
    // },
  },
  // ...
}
```

## 4、增加 router.js

```router.js
import {createRouter, createWebHistory} from 'vue-router'
import Home from '../components/home/Home.vue'

const router = createRouter({
  history: createWebHistory(),
  routes: [
    // route -> routes
    {
      path: '/',
      name: 'home',
      component: Home,
    },
    {
      path: '/cards',
      name: 'cards',
　　　 component: () => import(/* webpackChunkName: 'Card' */ '../components/cards/Cards.vue'),
　　},
　],
})

export default router
```



## 5、修改 index.js

```index.js
import { createApp } from 'vue'
import App from './App.vue'
import './index.css'
//import store from './store'
import router from './router'

const app = createApp(App)

app.use(store)
app.use(router)
app.mount('#app')
```

