---
title: 使用vuepress-theme-reco搭建个人博客
date: 2020-08-08
tags: 
 - 博客
categories: 
 - vue
 - vuepress
publish: true
description: 当前页面的描述
sidebar: 'auto'
# sticky: 3
next: false
prev: vuepress-plugin
---
::: tip
vuepress-theme-reco 基于 vuepress 的博客主题
:::

# [vuepress-theme-reco](https://vuepress-theme-reco.recoluan.com/) 基于VuePress构建个人博客

## 初始化项目

```cmd
npx @vuepress-reco/theme-cli init <name>  
or
npm @vuepress-reco/theme-cli init <name>  
```

安装依赖

```cmd
cd <name>
npm install
```

## 启动项目

```cmd
npm run dev
```

## 添加插件

### 1、添加依赖

```cmd
npm i @vuepress-reco/vuepress-plugin-kan-ban-niang -D
```

### 2、在 config.js 增加plugin配置

```config.js
module.exports = {
    ...
    "plugins": [
        [
            "@vuepress-reco/vuepress-plugin-kan-ban-niang",
            {
                theme: ["shizuku", "z16", "haruto", "koharu", "izumi", "blackCat"],
                clean: true, //关闭控制面板
                width: 200,
                height: 400,
                messages: {
                    welcome: "我是***，欢迎你的关注",
                    home: "带你回家",
                    theme: "好吧，希望你能喜欢",
                    close: "再见哟"
                },
                modelStyle: {
                    left: '90px',
                    bottom: '-20px',
                    opacity: '0.9'
                },
                btnStyle: {
                    left: '90px',
                    bottom: '40px'
                }
            }
        ]
    ]
}

```