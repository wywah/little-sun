---
title: vuepress
date: 2020-08-08
tags: 
 - 博客
 - 文档
categories: 
 - vue
 - vuepress
publish: true
description: 当前页面的描述
sidebar: 'auto'
prev: false
next: vuepress-plugin
---

# vuepress 搭建静态文档

```cmd
mkdir blog && cd blog # Create an empty directory and go into it

yarn add vuepress @vuepress/theme-blog -D 
# Install the dependencies
# OR npm install vuepress @vuepress/theme-blog -D
```

1. **初始化开发环境:**

```
mkdir blog && cd blog

yarn init
```

2. **初始化后会在目录下生成一个 package.json 文件:**

```
# 下面内容均是 执行 yarn init 命令后设置的
{
  "name": "demo",
  "version": "1.0.0",
  "description": "vuepress 尝试",
  "main": "index.js",
  "author": "wh",
  "license": "ds"
}
```

3. **修改 package.json 增加 执行脚本:**

```
{
    "name": "demo",
    "version": "1.0.0",
    "description": "vuepress 尝试",
    "author": "wh",
    "license": "ds",
    # 执行脚本
    "scripts": {
        "dev": "vuepress dev docs",
        "build": "vuepress build docs"
    }
}
```

4. **添加 vuepress 依赖**

```
yarn add vuepress -D
```

> 目录结构：
>
> ```
> .
> ├── docs
> │   ├── .vuepress (可选的)
> │   │   ├── components (可选的)
> │   │   ├── theme (可选的)
> │   │   │   └── Layout.vue
> │   │   ├── public (可选的)
> │   │   ├── styles (可选的)
> │   │   │   ├── index.styl
> │   │   │   └── palette.styl
> │   │   ├── templates (可选的, 谨慎配置)
> │   │   │   ├── dev.html
> │   │   │   └── ssr.html
> │   │   ├── config.js (可选的)
> │   │   └── enhanceApp.js (可选的)
> │   │ 
> │   ├── README.md
> │   ├── guide
> │   │   └── README.md
> │   └── config.md
> │ 
> └── package.json
> ```
>
> 请留意目录名的大写
>
> - `docs/.vuepress`: 用于存放全局的配置、组件、静态资源等。
> - `docs/.vuepress/components`: 该目录中的 Vue 组件将会被自动注册为全局组件。
> - `docs/.vuepress/theme`: 用于存放本地主题。
> - `docs/.vuepress/styles`: 用于存放样式相关的文件。
> - `docs/.vuepress/styles/index.styl`: 将会被自动应用的全局样式文件，会生成在最终的 CSS 文件结尾，具有比默认样式更高的优先级。
> - `docs/.vuepress/styles/palette.styl`: 用于重写默认颜色常量，或者设置新的 stylus 颜色常量。
> - `docs/.vuepress/public`: 静态资源目录。
> - `docs/.vuepress/templates`: 存储 HTML 模板文件。
> - `docs/.vuepress/templates/dev.html`: 用于开发环境的 HTML 模板文件。
> - `docs/.vuepress/templates/ssr.html`: 构建时基于 Vue SSR 的 HTML 模板文件。
> - `docs/.vuepress/config.js`: 配置文件的入口文件，也可以是 `YML` 或 `toml`。
> - `docs/.vuepress/enhanceApp.js`: 客户端应用的增强。
>
> 详细查看[官网说明](https://www.vuepress.cn/guide/directory-structure.html#%E9%BB%98%E8%AE%A4%E7%9A%84%E9%A1%B5%E9%9D%A2%E8%B7%AF%E7%94%B1)

5. **完善目录结构：**

```
·
├─docs
│  └─.vuepress
│      └─public
└─node_modules
```

6. 编写文档主页 README.md

```
---
home: true
heroImage: /logo.png
heroText: xxx文档
tagline: xxx
actionText: 快速上手 →
actionLink: /zh/guide/
features:
- title: 简洁至上
  details: 以 Markdown 为中心的项目结构，以最少的配置帮助你专注于写作。
- title: Vue驱动
  details: 享受 Vue + webpack 的开发体验，在 Markdown 中使用 Vue 组件，同时可以使用 Vue 来开发自定义主题。
- title: 高性能
  details: VuePress 为每个页面预渲染生成静态的 HTML，同时在页面被加载的时候，将作为 SPA 运行。
footer: MIT Licensed | Copyright © 2020-present Evan You
---
```

