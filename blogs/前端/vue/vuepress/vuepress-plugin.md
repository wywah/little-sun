---
title: vuepress plugin 开发
date: 2020-08-09
tags: 
 - 博客
 - 文档
categories: 
 - vue
 - vuepress
publish: true
description: 当前页面的描述
sidebar: 'auto'
prev: vuepress
next: vuepress-theme-reco搭建个人博客
---
:::tip
[vuepress plugin 官网API](https://vuepress.vuejs.org/zh/plugin/);
插件通常会为 VuePress 添加全局功能。插件的范围没有限制。你可以在 [Awesome VuePress](https://github.com/vuepressjs/awesome-vuepress#plugins) 中找到更多的插件 
:::

## 官方插件vuepress-plugin-back-to-up结构

```tree
|-- plugin-back-to-up
   |-- BackToUp.vue  //vue组件
   |-- enhanceAppFile.js  //把组件注册vuepress的全局中，并使用
   |-- index.js  // vuepress-plugin 入口文件
```

```index.js
// 导出一个普通的 JavaScript 对象
const { path } = require('@vuepress/shared-utils')
module.exports = {
  enhanceAppFiles: [
    path.resolve(__dirname, 'enhanceAppFile.js')
  ],
  globalUIComponents: 'BackToTop'
}
```

```enhanceAppFile.js
import BackToTop from './BackToTop.vue' //导入Vue组件
export default ({ Vue }) => {
  Vue.component('BackToTop', BackToTop)
}
```

## 自定义 press-plugin-test-demo插件

```tree
-- 基于 Option API 开发插件
|-- .vuepress
	|-- plugins
        |-- plugin-test-demo
           |-- bin
               |-- TestDemo.vue  //vue组件
               |-- enhanceAppFile.js  //把组件注册vuepress的全局中，并使用
           |-- index.js  // vuepress-plugin 入口文件
    |-- config.js
```



### 1、创建TestDemo.vue组件

```vue
<template>
  <transition name="fade">
    <div class="floating_main" v-show=isShow>
        {{message}}
    </div>
  </transition>
</template>

<script>
export default {
  name:"TestDemo",
  data(){
    return {
      message:MESSAGE, //获取 optionApi中的参数
      isShow: SHOW  //获取 optionApi中的参数
    }
  }
}
</script>

<style>
  .floating_main {
    z-index: 9999;
    color: red;
    font-size: 30px;
    position: absolute;
    top: 154px;
    left: 20px;
    right: 20px;
    text-align: center;
  }
</style>
```

### 2、编写 enhanceAppFile.js文件，注入该组件

```enhanceAppFile.js
import TestDemo from './TestDemo'
export default ({ Vue }) => {
    Vue.component('TestDemo', TestDemo)
}
```

### 3、插件入口程序 index.js

```index.js
const { path } = require('@vuepress/shared-utils')
module.exports = (options, context) => ({
    name: "vuepress-plugin-test-demo",
    define() {
        const { icon, message, show } = options
        return {
            ICON: icon || 'reco-up',
            MESSAGE: message || '测试 vuepress-plugin-test-demo',
            SHOW: show || false
        }
    },
    enhanceAppFiles: [
        path.resolve(__dirname, './bin/enhanceAppFile.js')
    ],
    globalUIComponents: 'TestDemo',
    async ready() {
    	// 生命周期函数
    	// ready 钩子在应用初始化之后，并在某些特定的函数式 API 执行之前执行。
    	// 这些函数式 API 包括： clientDynamicModules 、enhanceAppFiles
    },
    updated() {
   		// 生命周期函数
    },
    async generated (pagePaths) {
    	// 生命周期函数
     	//在生产环境的构建结束后被调用，生成的页面的路径数组将作为该函数的第一个参数
    }
})
```

### 使用 press-plugin-test-demo 插件

```config.js
// .vuepress/config.js 增加plugin配置 如下图：
{
....
	plugin:{
		[require('../../plugins/plugin-test-demo/index'), {
        	show: true, //显示插件
        	message: "测试哟" //提示
    	}]
	}
}
```

