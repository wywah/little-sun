const plugins = require('./config/plugins/')
const sidebar = require('./config/sidebar/')
const nav = require('./config/nav/')
module.exports = {
    "base": "/little-sun/",
    "title": "爱慧",
    "description": "个人博客",
    "dest": "src",
    "head": [
        [
            "link",
            {
                "rel": "icon",
                "href": "./favicon.ico"
            }
        ],
        [
            "meta",
            {
                "name": "viewport",
                "content": "width=device-width,initial-scale=1,user-scalable=no"
            }
        ],
        [
            "link",
            {
                "rel": "stylesheet",
                "href": "http://at.alicdn.com/t/font_2351669_hid4pzs0brv.css"
            }
        ]
    ],
    locales: {
        '/': {
            lang: 'zh-CN'
        }
    },
    "theme": "reco",
    "themeConfig": {
        codeTheme: "okaidia",
        nav,
        sidebar,
        "type": "blog",
        "blogConfig": {
            "category": {
                "location": 2
            },
            "tag": {
                "location": 3
            }
        },
        "friendLink": [{
                "title": "午后南杂",
                "desc": "Enjoy when you can, and endure when you must.",
                "email": "1156743527@qq.com",
                "link": "https://www.recoluan.com"
            },
            {
                "title": "vuepress-theme-reco",
                "desc": "A simple and beautiful vuepress Blog & Doc theme.",
                "avatar": "https://vuepress-theme-reco.recoluan.com/icon_vuepress_reco.png",
                "link": "https://vuepress-theme-reco.recoluan.com"
            }
        ],
        sidebarDepth: 3,
        "logo": "/logo.jpg",
        "search": true,
        "searchMaxSuggestions": 10,
        "lastUpdated": true,
        "lastUpdated": "更新时间",
        "author": "wangyw",
        "authorAvatar": "./avatar.jpg",
        // 备案
        "record": 'ICP 备案文案',
        "recordLink": 'ICP 备案指向链接',
        "cyberSecurityRecord": '公安部备案文案',
        "cyberSecurityLink": '公安部备案指向链接',
        "startYear": "2020"
    },
    "markdown": {
        "lineNumbers": false
    },
    plugins
}