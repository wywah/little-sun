module.exports = [
    [
        "@vuepress-reco/vuepress-plugin-kan-ban-niang",
        {
            theme: ["shizuku", "z16", "haruto", "koharu", "izumi", "blackCat"],
            clean: false, //关闭控制面板
            width: 200,
            height: 400,
            messages: {
                welcome: "我是***，欢迎你的关注",
                home: "带你回家",
                theme: "好吧，希望你能喜欢",
                close: "再见哟"
            },
            modelStyle: {
                left: '90px',
                bottom: '-20px',
                opacity: '0.9'
            },
            messageStyle: {
                left: '100px',
                bottom: '300px'
            },
            btnStyle: {
                left: '57px',
                bottom: '40px'
            }
        }
    ],
    // require('../../plugins/musicPlayer/index'),
    [require('../../plugins/notification/index'), {
        title: '欢迎一起学习!!🎉🎉🎉',
        content: '查看源码'
    }],
    "vuepress-plugin-boxx", ["vuepress-plugin-nuggets-style-copy", {
        copyText: "复制代码",
        tip: {
            content: "复制成功!"
        }
    }],
    ['@vuepress/last-updated', { // "上次更新"时间格式
        transformer: (timestamp, lang) => {
            const moment = require('moment') // https://momentjs.com/
            return moment(timestamp).format('YYYY/MM/DD, H:MM:SS');
        }
    }],
    [require('../../plugins/floatingTag/index'), {
        show: false,
        message: "测试哟"
    }]
]