//导航栏
module.exports = [

    { text: '主页', link: '/index.html', icon: 'reco-home' },
    { text: '时间线', link: '/timeline/', icon: 'reco-date' },
    {
        text: '个人笔记',
        icon: 'reco-document',
        items: [
            { text: 'spring', icon: 'ls-spring', link: '/spring/' },
            { text: 'springboot', icon: 'ls-spring-boot', link: '/springboot/' },
            { text: 'springcloud', icon: 'ls-springcloud', link: '/springcloud/' },
            { text: 'Python', icon: 'ls-python', link: '/pythons/' },
            { text: 'IDEA', link: '/idea/' },
            { text: 'JAVA', link: '/java/' },
            { text: '正则表达式', link: '/regular_expression/' },
            {
                text: '前端',
                icon: 'ls-h',
                items: [
                    { text: 'CSS', icon: 'ls-css3', link: '/前端/css/' },
                ]
            },
        ]
    },
    {
        text: '关于我',
        icon: 'reco-message',
        items: [
            { text: '码云', link: 'https://gitee.com/wyw_miss', icon: 'reco-mayun' }
        ]
    }
]