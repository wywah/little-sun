//侧边栏
module.exports = {
    "/spring/": [
        "",
    ],
    "/springboot/": [
        "", "conditionalAnnotation","spring-analysis","extensionPoints","spring-validator","spring-boot-starters", "knife4j", "springBootOptimization1"
    ],
    "/springcloud/": [
        "",
        "sping-cloud-alibaba-nacos/sping-cloud-alibaba-nacos",
        "sping-cloud-gateway/gateway-swagger",
        "sping-cloud-gateway/sping-cloud-gateway"
    ],
    "/database/": [
        "", "oracle/sort", "oracle/ranking", "oracle/substr1"
    ],
    "/pythons/": [
        "", "unp"
    ],
    "/idea/": [
        ""
    ],
    "/java/": [
        "",
        "jvm/JVM优化"
    ],
    "/前端/css/": [
        "", "iconfont/"
    ]
}