const { path } = require('@vuepress/shared-utils')

module.exports = (options, context) => ({
    define() {
        const { icon, customStyle, visibilityHeight, imgAlt, title, content } = options
        return {
            TITLE_: title || '欢迎一起Go!! 🎉🎉🎉',
            CONTENT_: content || '该功能为测试功能',
            ICON: icon || 'reco-up',
            ALT: imgAlt || "my",
            CUSTOM_STYLE: customStyle || {
                right: '1rem',
                bottom: '6rem',
                width: '2.5rem',
                height: '2.5rem',
                'border-radius': '.25rem',
                'line-height': '2.5rem',
                backgroundColor: 'rgba(231, 234, 241,.5)'
            },
            VISIBILITY_HEIGHT: visibilityHeight || 400
        }
    },
    name: '@vuepress-reco/vuepress-plugin-notification',
    enhanceAppFiles: [
        path.resolve(__dirname, './bin/enhanceAppFile.js')
    ],
    globalUIComponents: 'Notification'
})