const { path } = require('@vuepress/shared-utils')
module.exports = (options, context) => ({
    name: "vuepress-plugin-floatingTag",
    define() {
        const { icon, message, show } = options
        return {
            ICON: icon || 'reco-up',
            MESSAGE: message || '测试 vuepress-plugin-floatingTag',
            SHOW: show || false
        }
    },
    enhanceAppFiles: [
        path.resolve(__dirname, './bin/enhanceAppFile.js')
    ],
    globalUIComponents: 'floatingTag'
})