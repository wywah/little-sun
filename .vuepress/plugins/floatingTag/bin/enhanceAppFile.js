import floatingTag from './floatingTag'
export default ({ Vue }) => {
    Vue.component('floatingTag', floatingTag)
}