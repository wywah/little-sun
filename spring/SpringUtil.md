---
title: Spring 提供工具
date: 2022-03-02
tags: 
 - Spring
 - SpringBoot
categories: 
 - Spring
 - SpringBoot
description: Spring 提供工具
sidebar: 'auto'
---
# Spring 提供的各种工具
> 基于 Spring-5.3.x 版本

## 路径匹配工具
- AntPathMatcher