# Python开发环境搭建

## 安装 Python
1. 下载 python 

   Python官网：https://www.python.org/

   你可以在以下链接中下载 Python 的文档，你可以下载 HTML、PDF 和 PostScript 等格式的文档。

   Python文档下载地址：https://www.python.org/doc/

   ![下载1](./images/py_download.png)

   ![down2](./images/py_download2.png)

   > 我这里是下载的windows installer 64-bit 安装包 

2. 安装 python

    点击python安装文件 ， 一路next即可。

    ![install1](./images/py_install_1.png)

    ![install2](./images/py_install_2.png)

    **设置环境变量**：
    ![env](./images/py_env.png)

​	 ```
        path;e:python\python3.9.1
    ```

    

3.  检查是否安装成功

    ```
    # 查看 python 版本号 ， 如正常输出 python 版本号 ，即为安装成功。
    python -V
    ```
## 使用 PyCharm 创建第一个项目

1. 设置项目名称和python解释器
    ![create_1](./images/py_create_project_1.png)
    ![create_2](./images/py_create_project_2.png)
2. 创建第一个Python文件
    > 打印 Hello World
    ```Main.py
        # 声明一个函数
        def info(msg):
            print(f'Hello World! {msg}')

        # python main function
        if __name__ == '__main__':
            info('这是我第一个Pythonchen程序！')
    ```

3. 运行文件
    > 鼠标放在需要运行的文件或者文件中右击选择 Run / Debug 即可。
    ![run](./images/py_create_project_run.png)